import React from 'react';
import { Grid, Container, Typography, Box } from '@material-ui/core';


export const Feature = () => (
<div style={{backgroundColor: "#9e8fb2ff"}}>
<Box pt={5}>
<Container maxWidth="sm">
<Grid container>
<Grid item md={4} sm={1}>
<Box>
<img align="center" src={require('./HomeFeature/images/james.png')} style={{ width: "100%", height: "auto", maxWidth: "145px" }} alt="Logo" />
</Box>
</Grid>
<Grid item md={8} sm={1}>
<Box display="flex">
<Typography align="center" variant="h3" justifyContent="flex-end" style={{color: 'white'}}>Hello, I'm James.</Typography>
</Box>
<Box display="flex" pb={5}>
<Typography variant="h5" justifyContent="flex-end" style={{color: 'white'}}>Mechanical Engineering Graduate.<br />Computer Science Masters.</Typography>
</Box>
</Grid>
</Grid>
</Container>
</Box>
</div>
)

export default Feature;
import React from 'react';
import { Grid, Card, Container, CardContent, CardMedia, CardActionArea, Typography, CardActions, Button, Box } from '@material-ui/core';

export const CurrentProjects = () => (
    <div style={{ padding: "20px", background: "#82aeb1ff" }}>
    <Box pb={3}>
    <img src={require('./HomeFeature/images/logo-icon.png')} style={{ width: "8%", height: "auto", maxWidth: "50px" }} alt="Logo" />
        <Typography variant="h5" style={{ color: "white" }}>Current Projects</Typography>
        </Box>
        <Grid container spacing={3}>
            <Grid item sm={1} md={4}>
            

                <Card variant="outlined">
                    <CardActionArea>
                        <CardMedia
                            component="img"
                            alt="Live Services Rota"
                            height="140"
                            image={require('./CurrentProjects/images/sage.png')}
                            title="Live Services Rota"
                            style={{ height: "300px" }}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h7" component="h2">
                                Live Services Rota
              </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                As part of an assignment for my Software Engineering module, we were asked to produce
                                a rota system for Sage Live Services.
              </Typography>
                        </CardContent>
                    </CardActionArea>

                </Card>


            </Grid>
            <Grid item sm={1} md={4}>
            
            <Card variant="outlined">
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="Live Services Rota"
                    height="140"
                    image={require('./CurrentProjects/images/sage.png')}
                    title="Live Services Rota"
                    style={{ height: "300px" }}
                />
                <CardContent>
                    <Typography gutterBottom variant="h7" component="h2">
                        Live Services Rota
      </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        As part of an assignment for my Software Engineering module, we were asked to produce
                        a rota system for Sage Live Services.
      </Typography>
                </CardContent>
            </CardActionArea>

        </Card>

            </Grid>


            <Grid item sm={1} md={4}>
            
            <Card variant="outlined">
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="Live Services Rota"
                    height="140"
                    image={require('./CurrentProjects/images/sage.png')}
                    title="Live Services Rota"
                    style={{ height: "300px" }}
                />
                <CardContent>
                    <Typography gutterBottom variant="h7" component="h2">
                        Live Services Rota
      </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        As part of an assignment for my Software Engineering module, we were asked to produce
                        a rota system for Sage Live Services.
      </Typography>
                </CardContent>
            </CardActionArea>

        </Card>

            </Grid>
            </Grid>
       
    </div>
)

export default CurrentProjects;
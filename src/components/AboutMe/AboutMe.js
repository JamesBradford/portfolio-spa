import React from 'react';
import { Typography, Container, Box, List, ListItem, ListItemIcon, Grid } from '@material-ui/core';
import CodeIcon from '@material-ui/icons/Code';

export const AboutMe = () =>
  (
    <div>
      <Container sm>

        <Box py={3}>
              <Typography variant="h4">About Me</Typography>
              <Typography variant="p">
                I am a graduate Mechanical Engineer currently studying a Computer Science Master’s degree.  I am passionate about Web and Software Development. 
    </Typography>
              <Box pt={2}>
                <Typography variant="h5">Key Skills</Typography>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <CodeIcon />
                    </ListItemIcon>
                    <Typography variant="p">Java</Typography>
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CodeIcon />
                    </ListItemIcon>
                    <Typography variant="p">HTML, CSS, JavaScript</Typography>
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CodeIcon />
                    </ListItemIcon>
                    <Typography variant="p">PHP</Typography>
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CodeIcon />
                    </ListItemIcon>
                    <Typography variant="p">React</Typography>
                  </ListItem>
                </List>
              </Box>
        </Box>

      </Container>
    </div>
  )

export default AboutMe;
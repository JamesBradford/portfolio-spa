import React from 'react';
import { IconButton, Typography, Container, Box, Grid } from '@material-ui/core';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { Icon, Link } from '@iconify/react';
import gitlabicon from '@iconify/icons-mdi/gitlab'

export const Socials = () => (
<div>
<Container sm>
<Box py={3}>
<Typography variant="h5">Get in touch</Typography>

<Grid container spacing={1}>
<Grid item>
<a href="https://www.linkedin.com/in/james-bradford-291268132">
    <IconButton style={{ backgroundColor: "#9e8fb2ff"}}>
        <LinkedInIcon style={{ color: "white" }} />
    </IconButton>
    </a>
    </Grid>
    <Grid item>
    <a href="https://gitlab.com/JamesBradford">
    <IconButton style={{ backgroundColor: "#9e8fb2ff"}}>
    <Icon icon={gitlabicon} style={{ color: "white" }} />
</IconButton>
</a>
</Grid>
</Grid>

    </Box>
    </Container>
    </div>
)

export default Socials;
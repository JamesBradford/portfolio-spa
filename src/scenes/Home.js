import React from 'react';
import HomeFeature from '../components/HomeFeature';
import CurrentProjects from '../components/CurrentProjects';
import Socials from '../components/Socials/Socials';
import AboutMe from '../components/AboutMe/AboutMe';

export const HomeScene = () => (
    <div>

    <br />
    <br />
    <br />
    <HomeFeature/>
    <AboutMe/>
    <CurrentProjects/>
    <Socials/>
    </div>
)

export default HomeScene;